import cv2
import numpy as np
from imutils.perspective import four_point_transform
import socket
from keras.models import model_from_json
import imutils

class NeuralNetwork(object):

    def __init__(self):
        pass

    def create(self):
        # self.model = cv2.ml.ANN_MLP_load('mlp_xml/mlp.xml')

        json_file = open('nvidia_model.json', 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        self.model = model_from_json(loaded_model_json)
        self.model.load_weights('nvidia_model_weights.h5')

    def predict(self, samples):
        # ret, resp = self.model.predict(samples)
        # return resp.argmax(-1)

        resp = self.model.predict_classes(samples)[0]
        return resp


class RCControl(object):

    def __init__(self):
        pass

    def steer(self, prediction):
        if prediction == 2:
            print("Forward")
            return 'w'

        elif prediction == 0:
            print("Left")
            return 'a'

        elif prediction == 1:
            print("Right")
            return 'd'

        elif prediction == 3:
            print("Reverse")
            return 'x'

        else:
            return 's'


class VideoStreamHandler(object):

    def __init__(self):
        self.server_socket = socket.socket()
        self.server_socket.bind(('192.168.0.90', 5003))
        self.server_socket.listen(0)
        self.connection = self.server_socket.accept()[0].makefile('rb')
        self.client_socket = socket.socket()
        self.client_socket.connect(('192.168.0.88', 50007))
        # self.client_socket.connect(('192.168.137.124', 50007))
        self.handle()
        self.identifyTrafficSign()

    # create neural network
    model = NeuralNetwork()
    model.create()

    rc_car = RCControl()

    def identifyTrafficSign(self,image):
        '''
        In this function we select some ROI in which we expect to have the sign parts. If the ROI has more active pixels than threshold we mark it as 1, else 0
        After path through all four regions, we compare the tuple of ones and zeros with keys in dictionary SIGNS_LOOKUP
        '''




        # define the dictionary of signs segments so we can identify
        # each signs on the image
        SIGNS_LOOKUP = {
            (1, 0, 0, 1): 'Turn Right',  # turnRight
            (0, 0, 1, 1): 'Turn Left',  # turnLeft
            (0, 1, 0, 1): 'Move Straight',  # moveStraight
            (1, 0, 1, 1): 'Turn Back',  # turnBack
        }



        THRESHOLD = 150

        image = cv2.bitwise_not(image)
        # (roiH, roiW) = roi.shape
        # subHeight = thresh.shape[0]/10
        # subWidth = thresh.shape[1]/10
        (subHeight, subWidth) = np.divide(image.shape, 10)
        subHeight = int(subHeight)
        subWidth = int(subWidth)

        # mark the ROIs borders on the image
        cv2.rectangle(image, (subWidth, 4 * subHeight), (3 * subWidth, 9 * subHeight), (0, 255, 0), 2)  # left block
        cv2.rectangle(image, (4 * subWidth, 4 * subHeight), (6 * subWidth, 9 * subHeight), (0, 255, 0),
                      2)  # center block
        cv2.rectangle(image, (7 * subWidth, 4 * subHeight), (9 * subWidth, 9 * subHeight), (0, 255, 0),
                      2)  # right block
        cv2.rectangle(image, (3 * subWidth, 2 * subHeight), (7 * subWidth, 4 * subHeight), (0, 255, 0), 2)  # top block

        # substract 4 ROI of the sign thresh image
        leftBlock = image[4 * subHeight:9 * subHeight, subWidth:3 * subWidth]
        centerBlock = image[4 * subHeight:9 * subHeight, 4 * subWidth:6 * subWidth]
        rightBlock = image[4 * subHeight:9 * subHeight, 7 * subWidth:9 * subWidth]
        topBlock = image[2 * subHeight:4 * subHeight, 3 * subWidth:7 * subWidth]

        # we now track the fraction of each ROI
        leftFraction = np.sum(leftBlock) / (leftBlock.shape[0] * leftBlock.shape[1])
        centerFraction = np.sum(centerBlock) / (centerBlock.shape[0] * centerBlock.shape[1])
        rightFraction = np.sum(rightBlock) / (rightBlock.shape[0] * rightBlock.shape[1])
        topFraction = np.sum(topBlock) / (topBlock.shape[0] * topBlock.shape[1])

        segments = (leftFraction, centerFraction, rightFraction, topFraction)
        segments = tuple(1 if segment > THRESHOLD else 0 for segment in segments)

        cv2.imshow("Warped", image)

        if segments in SIGNS_LOOKUP:
            return SIGNS_LOOKUP[segments]
        else:
            return None


    def handle(self):
        stream_bytes = ' '

        lower = {'red': (120, 54, 101), 'green': (66, 122, 129),
                 'yellow': (23, 59, 119)}  # assign new item lower['blue'] = (93, 10, 0)
        # lower = {'red': (50, 30, 141), 'green': (66, 122, 129),
        #          'yellow': (23, 59, 119)}  # assign new item lower['blue'] = (93, 10, 0)
        upper = {'red': (200, 255, 255), 'green': (86, 255, 255), 'yellow': (54, 255, 255)}
        colors = {'red': (0, 0, 255), 'green': (0, 255, 0), 'yellow': (0, 255, 217)}
        lower_blue = np.array([85, 100, 70])
        upper_blue = np.array([115, 255, 255])



        def steer(self, prediction):
            if prediction == 2:
                print("Forward")
                return 'w'

            elif prediction == 0:
                print("Left")
                return 'a'

            elif prediction == 1:
                print("Right")
                return 'd'

            elif prediction == 3:
                print("Reverse")
                return 'x'

            else:
                return 's'

        # stream video frames one by one
        # camera = cv2.VideoCapture(0)  # hhh
        try:

            while True:
                # (grabbed, frame) = camera.read()#hhh
                # blurred = cv2.GaussianBlur(frame, (11, 11), 0)#hhh
                # hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)#hhh
                # cv2.imshow("Frame", frame)#hhh

                stream_bytes += self.connection.read(1024)
                first = stream_bytes.find('\xff\xd8')
                last = stream_bytes.find('\xff\xd9')
                if first != -1 and last != -1:
                    jpg = stream_bytes[first:last + 2]
                    stream_bytes = stream_bytes[last + 2:]
                    gray = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.IMREAD_GRAYSCALE)

                    img = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.IMREAD_UNCHANGED)
                    blurred = cv2.GaussianBlur(img, (11, 11), 0)
                    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)


                    imgArea=img.shape[0]*img.shape[1]
                    hsv_1=  cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
                    kernel = np.ones((3, 3), np.uint8)
                    mask = cv2.inRange(hsv_1, lower_blue, upper_blue)
                    # morphological operations
                    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
                    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
                    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]

                    detectedTrafficSign = None
                    largestArea = 0
                    largestRect = None

                    # only proceed if at least one contour was found
                    if len(cnts) > 0:
                        for cnt in cnts:
                            # Rotated Rectangle. Here, bounding rectangle is drawn with minimum area,
                            # so it considers the rotation also. The function used is cv2.minAreaRect().
                            # It returns a Box2D structure which contains following detals -
                            # ( center (x,y), (width, height), angle of rotation ).
                            # But to draw this rectangle, we need 4 corners of the rectangle.
                            # It is obtained by the function cv2.boxPoints()
                            rect = cv2.minAreaRect(cnt)
                            box = cv2.boxPoints(rect)
                            box = np.int0(box)

                            # count euclidian distance for each side of the rectangle
                            sideOne = np.linalg.norm(box[0] - box[1])
                            sideTwo = np.linalg.norm(box[0] - box[3])
                            # count area of the rectangle
                            area = sideOne * sideTwo
                            # find the largest rectangle within all contours
                            if area > largestArea:
                                largestArea = area
                                largestRect = box

                    # draw contour of the found rectangle on  the original image
                    if largestArea > imgArea * 0.02:
                        cv2.drawContours(img, [largestRect], 0, (0, 0, 255), 2)

                        # if largestRect is not None:
                        # cut and warp interesting area
                        warped = four_point_transform(mask, [largestRect][0])
                        print warped
                        # show an image if rectangle was found
                        # cv2.imshow("Warped", cv2.bitwise_not(warped))

                        # use function to detect the sign on the found rectangle
                        detectedTrafficSign = self.identifyTrafficSign(warped)
                        print(detectedTrafficSign)

                        # write the description of the sign on the original image
                        cv2.putText(img, detectedTrafficSign, tuple(largestRect[0]), cv2.FONT_HERSHEY_SIMPLEX, 0.65,
                                    (0, 255, 0), 2)

                    # show original image
                    frame = cv2.resize(img, (320, 120))
                    cv2.imshow("Original", frame)

                    # if the `q` key was pressed, break from the loop
                    if cv2.waitKey(1) & 0xFF is ord('q'):
                        cv2.destroyAllWindows()
                        print("Stop programm and close all windows")
                        break

                    for key, value in upper.items():
                        # construct a mask for the color from dictionary`1, then perform
                        # a series of dilations and erosions to remove any small
                        # blobs left in the mask
                        kernel = np.ones((9, 9), np.uint8)
                        mask = cv2.inRange(hsv, lower[key], upper[key])
                        mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
                        mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)

                        # find contours in the mask and initialize the current
                        # (x, y) center of the ball
                        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
                                                cv2.CHAIN_APPROX_SIMPLE)[-2]
                        # center = None

                        # only proceed if at least one contour was found
                        # if len(cnts) > 0:
                        #     # find the largest contour in the mask, then use
                        #     # it to compute the minimum enclosing circle and
                        #     # centroid
                        #     c = max(cnts, key=cv2.contourArea)
                        #     ((x, y), radius) = cv2.minEnclosingCircle(c)
                        #     M = cv2.moments(c)
                        #     # center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
                        #
                        #     # only proceed if the radius meets a minimum size. Correct this value for your obect's size
                        #     if radius > 30 and radius <50:
                        #         # draw the circle and centroid on the frame,
                        #         # then update the list of tracked points
                        #         cv2.circle(img, (int(x), int(y)), int(radius), colors[key], 2)
                        #         cv2.putText(img, key, (int(x - radius), int(y - radius)), cv2.FONT_HERSHEY_SIMPLEX,
                        #                     0.6, colors[key], 2)
                        #         if colors[key] == (0, 0, 255):
                        #             print ('RED')
                        #
                        #         elif colors[key] == (0, 255, 0):
                        #             print ('GREEN')
                        #         elif colors[key] == (0, 255, 217):
                        #             print ('ORANGE')

                    cv2.imshow("Frame", img)
                    # lower half of the image
                    half_gray = gray[120:240, :]

                    # cv2.imshow('image', image)
                    cv2.imshow('image', half_gray)
                    # cv2.imshow('image', gray)
                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        break
                    # reshape image
                    image_array = half_gray.reshape(-1, 120, 320, 1).astype(np.float32)

                    # neural network makes prediction
                    prediction = self.model.predict(image_array)
                    direction = self.rc_car.steer(prediction)
                    print direction

                    if direction not in ['w', 'a', 's', 'd', 'x']:
                        continue
                    if detectedTrafficSign =='Turn Left':
                        direction='a'
                    elif detectedTrafficSign =='Turn Right':
                        direction='d'
                    elif detectedTrafficSign =='Turn Back':
                        direction='x'


                    if len(cnts) > 0:

                        c = max(cnts, key=cv2.contourArea)
                        ((x, y), radius) = cv2.minEnclosingCircle(c)

                        if radius > 30 and radius < 50:

                            cv2.circle(img, (int(x), int(y)), int(radius), colors[key], 2)
                            cv2.putText(img, key, (int(x - radius), int(y - radius)), cv2.FONT_HERSHEY_SIMPLEX, 0.6,
                                        colors[key], 2)
                            if colors[key] == (0, 0, 255):
                                print ('RED')
                                direction = 's'
                            elif colors[key] == (0, 255, 0):
                                print ('GREEN')
                            elif colors[key] == (0, 255, 217):
                                print ('ORANGE')

                    self.client_socket.sendall(direction.encode())

            cv2.destroyAllWindows()

        finally:
            self.connection.close()
            self.server_socket.close()
            self.client_socket.close()


if __name__ == '__main__':
    VideoStreamHandler()

